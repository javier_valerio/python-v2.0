# Rabbit Dump
# This will hit the cluster specified and dump the current overview, connections, channels, consumers
# It will dump these as json files in a directory based on the servername and current date
# Execute by calling RabbitDump.sh with the cluster name that you want to hit
#   ./RabbitDump.sh us-west-1-c1

RAW_INPUT=""
Proceed(){
	echo "Would you like to Proceed? (Y/N)"
	read input
	#This will convert upper-case letters to lower-case letters
	input="$(sed y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/ <<< "$input")"
	RAW_INPUT="$input"
}

if [ "$#" -eq 1 ] && [ "$1" == "-h" ]
then
	echo "****************************************************************************"
	echo " Will dump overview, connections, channels, and consumers into a json file"
	echo "****************************************************************************"
	echo "usage: $0 <cluster>"
	echo "example: $0 us-west-2-dev"
	exit 0
fi

if [ "$#" -lt 1 ]
then
	echo "Expected 1 arguments for $0"
	exit 0
fi

SERVER="$1"
DATE=`date +%Y-%m-%d`
DIRECTORY=$SERVER-$DATE
# make parent directories as needed
mkdir -p $DIRECTORY

echo -e "\n======================================================================"
echo "Dumping rabbit stats of: $SERVER to: $DIRECTORY"
echo -e "======================================================================\n"

Proceed
if [ "$RAW_INPUT" != "yes" ] && [ "$RAW_INPUT" != "y" ]
then
	echo "exiting"
	exit 0
else
	echo ">Grabbing Overview"
	curl -u admin:adminadmin http://$SERVER.perspectium.net:15672/api/overview > $DIRECTORY/rmq-overview.json
	echo ">Grabbing Connections"
	curl -u admin:adminadmin http://$SERVER.perspectium.net:15672/api/connections > $DIRECTORY/rmq-connections.json
	echo ">Grabbing Channels"
	curl -u admin:adminadmin http://$SERVER.perspectium.net:15672/api/channels > $DIRECTORY/rmq-channels.json
	echo ">Grabbing Consumers"
	curl -u admin:adminadmin http://$SERVER.perspectium.net:15672/api/consumers > $DIRECTORY/rmq-consumers.json
	echo ">Grabbing Queues"
	curl -uadmin:adminadmin https://$SERVER.perspectium.net:15672/api/queues > $DIRECTORY/rmq-queues.json
fi
