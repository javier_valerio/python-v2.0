#!/usr/bin/env python

external_functions = 'Util.py'

import os
import time
import sys
sys.path.append("/Library/Frameworks/Python.framework/Versions/3.5/lib/python3.5/site-packages")
import pika
from Util import *

if len(sys.argv) == 2 and sys.argv[1] == '-h':
	showUsage("Writes queue messages to file without de-queuing from RabbitMQ",
                sys.argv[0], "<host> [<vHost/queue> | <queue>]")
	exit()

ExpectedArguments( len(sys.argv), sys.argv[0], 3)

host = sys.argv[1]
queue, vHost = getQueueVirtualHost(sys.argv[2])

print "Host:	" + host
print "VHost:	" + vHost
print "Queue:	" + queue

Proceed()
authenticateRabbitCredentials(host, vHost)

messages = 0
def callback(ch, method, properties, body):
	global messages

	text_file.write(body)
	text_file.write("\n")
	messages = messages + 1

if vHost == "/":
	filename = host+".Queue="+queue+".Time="+time.strftime("%d%m%y-%T",time.localtime())+".txt"
else:
	filename = host+",Host="+vHost+",Queue="+queue+",Time="+time.strftime("%d%m%y-%T",time.localtime())+".txt"

text_file = open(filename, "w")

#QMessages=getRabbitChannel().queue_declare(queue,passive=True).method.message_count
getRabbitChannel().basic_consume(callback, queue)

try:
	print(' [*] Waiting for messages. To exit press CTRL+C')
	getRabbitChannel().start_consuming()
except KeyboardInterrupt:
	print "\n------------------------------------------------------------------------------"
	print "Dumped: " + queue + " (" + str(messages) + " messages)"
	print "Saving output file: " + filename
	print "------------------------------------------------------------------------------"


getRabbitConnection().close()
text_file.close()

"""
Check queue length
[javiervalerio@ip-172-16-0-5
~]$ sudo rabbitmqctl list_queues -p "polk" | grep "psp.out.replicator.javiertest"
psp.out.replicator.javiertest	0
[javiervalerio@ip-172-16-0-5 ~]$ sudo rabbitmqctl list_queues -p "/" | grep "psp.out.replicator.javiertest"
psp.out.replicator.javiertest	300
[javiervalerio@ip-172-16-0-5 ~]$ sudo rabbitmqctl list_queues -p "/" name messages_ready | grep "psp.out.replicator.javiertest"
"""
