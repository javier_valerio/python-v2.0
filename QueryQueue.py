#!/usr/bin/env python

external_functions = 'Util.py'

import requests
import platform
import pika
import time
import sys
import json
import os
from Util import *

# Python QueryQueue <host> [<vHost/queue> | <queue>] max_reads
if len(sys.argv) == 2 and sys.argv[1] == '-h':
    showUsage("Displays statistics from a queue",
                sys.argv[0], "<host> [<vHost/queue> | <queue>] <max_reads>")
    exit()

ExpectedArguments( len(sys.argv), sys.argv[0], 4)

host = sys.argv[1]
queue, vHost = getQueueVirtualHost(sys.argv[2])

print "Host:	" + host
print "VHost:	" + vHost
print "Queue:	" + queue

Proceed()
authenticateRabbitCredentials(host, vHost)

messages=0
max_messages = 200000
if len(sys.argv) == 4 and sys.argv[3] != "":
	max_messages = int(sys.argv[3])

topics=dict()
names=dict()
dates=dict()
datevaluepairs=dict()
keys=dict()
keynamepairs=dict()

def callback(ch, method, properties, body):
    global queueMessages, messages

    messages += 1
    if (messages == max_messages) or (messages == queueMessages):
    	#print "\nRead " + str(messages) + " messages.  Closing the connection..."
    	getRabbitConnection().close()

    if (messages % 1000 == 0):
    	print str(messages/1000) + "K",
    	sys.stdout.flush()
    else:
    	if (messages % 200 == 0):
    		print("."),
    		sys.stdout.flush()

	j = json.loads(body)
	countMessage(j['topic'],topics)
	countMessage(j['name'],names)
	countMessage(j['psp_timestamp'][0:10],dates)
	countMessage(j['key'],keys)
	countMessage(j['key']+" ---> "+j['name'],keynamepairs)

queueMessages=getRabbitChannel().queue_declare(queue,passive=True).method.message_count
getRabbitChannel().basic_consume(callback,queue)

print "\n---------------- Queue Statistics ----------------"
print "Queue messages: " + str(queueMessages)
print "Host: " + host
print "Virtual Host: " + vHost
print "Queue:" + queue

# We need to create two new functions, one for consuming rabbit, one for closing connection
try:
	getRabbitChannel().start_consuming()
except KeyboardInterrupt:
    getRabbitChannel().stop_consuming()

try:
	getRabbitConnection().close()
except Exception as exception:
	pass

printBreakdown(names, "name")
printBreakdown(topics, "topic")
printBreakdown(dates, "date")
printBreakdown(keys,"key")
printBreakdown(keynamepairs, "Names by key")
