#!/usr/bin/env python
#  RMQExportDefinitions.py
#
#  Will export the definitions from the RabbitMQ and put it in a json file.
#
#  To run just call:
#    python RMQExportDefinitions.py us-west-2-dev
import requests
import json
import time
import sys
from Util import *

if len(sys.argv) == 2 and sys.argv[1] == '-h':
    showUsage(" Export Definitions from RabbitMQ",sys.argv[0],"<cluster>")
    print "--- example: python RMQExportDefinitions.py us-west-2-dev"
    #print "--- usage: Python RMQExportDefinitions.py -s"
    exit()

# Skip writing of main payload to file
writeToFile = True

#if len(sys.argv) == 2 and sys.argv[1] == "-s":
#    writeToFile = False

ExpectedArguments( len(sys.argv), sys.argv[0], 2)

# Update the host
if len(sys.argv) >= 2:
    cluster = sys.argv[1]
    host = cluster + ".perspectium.net"

authenticateRabbitCredentials(cluster, '/')
print "Host:    " + host
print "cluster:         " + cluster
print "username:        " + getRabbitUser()
print "password:        " + getRabbitPassword()
print "servlet:         " + getRabbitDefinitionServlet()
Proceed()

# Get Payload
requestStr = 'http://' + getRabbitUser() + ":" + getRabbitPassword() + "@" + host + getRabbitDefinitionServlet()
r = requests.get(requestStr)
allConnections = r.json()

# Dump all connections to the file
if writeToFile:
	allConnectionsFileName = host  + "_all_definitions.json"
	allConnectionsFile = open(allConnectionsFileName, "w")
	json.dump(allConnections, allConnectionsFile, indent=4, sort_keys=True)
	allConnectionsFile.close()

