#!/bin/sh

SUDO="/usr/bin/sudo"

if [ -z "$1" ]; then
	echo "Invalid cluster name: $1"
	exit 0
fi

login_ssh () {

  CLUSTER_NAME="${1}-monitor.perspectium.net"
  ssh $CLUSTER_NAME "cat /etc/motd > /dev/null"
  ssh $CLUSTER_NAME "$SUDO cat /etc/ansible/hosts | grep 'rabbitmquser\|rabbitmqpassword' | cut -f2 -d="
  #echo "$?"
}


login_ssh $1