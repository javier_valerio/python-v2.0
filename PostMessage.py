#!/usr/bin/env python

external_functions = 'Util.py'

import requests
import time
import sys
import json
import os
from Util import *

if len(sys.argv) == 2 and sys.argv[1] == '-h':
    showUsage("Writes messages from a text file to a queue",
                sys.argv[0], "<host> [<vHost/queue> | <queue>] <text_file>")
    exit()

ExpectedArguments( len(sys.argv), sys.argv[0], 4)

host = sys.argv[1]
filename = sys.argv[3]
queue, vHost = getQueueVirtualHost(sys.argv[2])
authenticateRabbitCredentials(host, vHost)

print "Host:	" + host
print "VHost:	" + vHost
print "Queue:	" + queue
print "File to read: " + filename

Proceed()

httpHeaders = {"Content-Type": "application/json", "psp_input_queue": queue, "psp_quser" : getRabbitUser(), "psp_qpassword": getRabbitPassword()}
messages = []
with open(filename) as f:
    messages = [line for line in f if line.strip()]

startTime = time.time()
r = requests.post(url = getMBSMultiInputAPI(), data = str(messages), headers=httpHeaders)
endTime = time.time()

print "Response: " + str(r)
print "Duration of Post: " + str(endTime - startTime) + " seconds"
print "Reading: " + str(os.path.getsize(filename)/1024) + "KB of data"
print "Number of Messages: " + str(len(messages))
