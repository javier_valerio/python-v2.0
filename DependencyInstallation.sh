#!/bin/bash

RAW_INPUT=""

input(){
	read input
	#This will convert upper-case letters to lower-case letter
	input="$(sed y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/ <<< "$input")"
	RAW_INPUT="$input"
}

echo "Would you like to install pip/pika, proceed? (Y/N)"
input

#The spaces matter here in shell script
if [ "$RAW_INPUT" != "yes" ] && [ "$RAW_INPUT" != "y" ]
then
	echo "Skipping pika installation"
else
	sudo yum install -y epel-release
	sudo yum install -y python-pip
	sudo pip install --upgrade pip
	sudo pip install pika
	#sudo -y pip install subprocess
	sudo yum update -y 
	pip --version
fi

echo "Would you like to install python, proceed? (Y/N)" 
input
if [ "$RAW_INPUT" != "yes" ] && [ "$RAW_INPUT" != "y" ]
then
	echo Skipping Python installation
else
	sudo yum install -y python
	sudo yum update -y
	python --version
fi

echo "Would you like to install requests, proceed? (Y/N)"
input
if [ "$RAW_INPUT" != "yes" ] && [ "$RAW_INPUT" != "y" ]
then
	echo Skipping requests installation
else
	sudo yum install -y python-requests
	sudo yum update -y
fi



# Works cited
# https://stackoverflow.com/questions/2264428/how-to-convert-a-string-to-lower-case-in-bash
# https://ryanstutorials.net/bash-scripting-tutorial/bash-if-statements.phpa
# https://www.shellscript.sh/functions.html
# http://docs.python-requests.org/en/v1.0.0/community/out-there/
