#!/usr/bin/env python
# ConsumeQueueToText.py
#   Go to the queue specified in the parameters and consume it all down to a file.
#   Messages will be de-queued from the queue as they are consumed
#
#   Can put queue into the file or call from the command line
#
#   ./ConsumeQueueToText.py psp.out.queue.to.consume
import time
import sys
sys.path.append("/Library/Frameworks/Python.framework/Versions/3.5/lib/python3.5/site-packages")
import pika
from Util import *

if len(sys.argv) == 2 and sys.argv[1] == '-h':
    showUsage(" Consumes from a queue and turn it into text file",sys.argv[0],"<cluster> [<queue>|<vHost>/<queue>]")
    print "--- example: python ConsumeQueueToText.py us-west-2-dev dev/psp.out.replicator.javiertest"
    exit()

ExpectedArguments( len(sys.argv), sys.argv[0], 3)

host= sys.argv[1]
queue, vHost = getQueueVirtualHost(sys.argv[2])

authenticateRabbitCredentials(host, vHost)
# print out result for user to double check
print "Host/Cluster:    " + host
print "Virtual Host:    " + vHost
print "Queue:           " + queue
Proceed()

# Callback for basic consume, write to file
def callback(ch, method, properties, body):
    text_file.write(body)
    text_file.write("\n")
    
text_file = open(host+"_"+queue+"_"+time.strftime("%d%m%y-%T",time.localtime())+".txt", "w")
getRabbitChannel().basic_consume(callback,queue, "True")

print(' [*] Waiting for messages. To exit press CTRL+C')

getRabbitChannel().start_consuming()
getRabbitConnection().close()
text_file.close()
