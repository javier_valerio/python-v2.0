#!/usr/bin/env python
# RMQLogQueues.py
#  Utility to log details of the queues in grabbed from RabbitMQ management api
#  Enter in the parameters below for host, username, and password
#
#  To tailor your output call variations:
#    printCount(allConnections, "connected_at")
#    printGroup(allConnections, "node", "username")
#  To run just call:
#    python RMQLogQueues.py us-west-2-dev
import requests
import json
import time
import sys
import os
from Util import *

if len(sys.argv) == 2 and sys.argv[1] == '-h':
    showUsage(" Export the connections from RabbitMQ",sys.argv[0],"<cluster>")
    print "--- example: python RMQLogConnections.py us-west-2-dev"
#    print "--- usage: python RMQLogConnections.py -s"
    exit()

ExpectedArguments( len(sys.argv), sys.argv[0], 2)

# Parameters
if len(sys.argv) >= 2:
    cluster     = sys.argv[1]
    host = cluster + ".perspectium.net"

authenticateRabbitCredentials(cluster, '/')
print "Host:        " + host
print "username:    " + getRabbitUser()
print "password:    " + getRabbitPassword()
print "servlet:     " + getRabbitQueuesServlet()
Proceed()
print "loading..."

# Get Payload
requestStr = 'http://' + getRabbitUser() + ":" + getRabbitPassword() + "@" + host + getRabbitQueuesServlet()
r = requests.get(requestStr)
allConnections = r.json()

# Skip writing of main payload to file
writeToFile = True
#if len(sys.argv) == 2 and sys.argv[1] == "-s":
#    writeToFile = False

# Dump all connections to the file
if writeToFile:
	allConnectionsFileName = host  + "_all_queues.json"
	allConnectionsFile = open(allConnectionsFileName, "w")
	json.dump(allConnections, allConnectionsFile, indent=4, sort_keys=True)
	allConnectionsFile.close()

# Place the output into a file
outputFileName = host + "_all_queues_output.json"
outputFile = open(outputFileName, "w")

# Printing tools
def printSeparator(str):
	print "\n========================================"
	print str
	print "========================================\n"
	outputFile.write("\n========================================")
	outputFile.write(str)
	outputFile.write("========================================\n")
	
def printJSONPretty(jsonObj):
	print json.dumps(jsonObj, indent=4, sort_keys=True)
	outputFile.write(json.dumps(jsonObj, indent=4, sort_keys=True))

# Sanitize username to vhost/username
def getUsername(jsonObj):
	#Sanitize username, idle_since, name
	if jsonObj["vhost"] == "/":
		return jsonObj["vhost"] + jsonObj["user"]
	else:
		return jsonObj["vhost"] + "/" + jsonObj["user"]
		
# Convert RMQ nanosecond epoch time to local time
def getLocalTime(jsonObj):
	if "idle_since" in jsonObj:
		return jsonObj["idle_since"]
	else:
		return "no_idle_time"
    #timeFormatShort = "%Y/%m/%d"
	#mEpochTime = jsonObj["idle_since"] / 1000
	#return time.strftime(timeFormatShort, time.localtime(mEpochTime))

# Sanitize the name field of the connection	
def getName(jsonObj):
	nameSplit = jsonObj["name"].split(" ")
	if len(nameSplit) >= 0:
		if len(nameSplit[0].split(":")) > 0:
			nameStr = nameSplit[0].split(":")[0]
		else:
			nameStr = nameSplit[0]
	if len(nameSplit) >= 2:
		nameStr += nameSplit[1]
	if len(nameSplit) >= 3:
		nameStr += nameSplit[2]	
	return nameStr

# Group by the elements specified in groupA and groupB
# Will print and return a 2D JSON of the counts of the grouping
def printGroupBy(connections, groupA, groupB):
	groupMaster = {}
	timeFormatShort = "%Y/%m/%d"
	for c in connections:
		#Sanitize username, idle_since, name
		sanitizedOutput = {}
		sanitizedOutput["username"] = getUsername(c)
		sanitizedOutput["idle_since"] = getLocalTime(c)
		sanitizedOutput["clean_name"] = getName(c)
				
		aVal = ""
		bVal = ""
		
		if groupA in c:
			aVal = c[groupA]
		if groupB in c:
			bVal = c[groupB]
		
		if groupA in sanitizedOutput:
			aVal = sanitizedOutput[groupA]
		if groupB in sanitizedOutput:
			bVal = sanitizedOutput[groupB]
			
		# Add to grouping	
		if aVal not in groupMaster:
			groupMaster[aVal] = {}
		
		if bVal in groupMaster[aVal]:
			groupMaster[aVal][bVal] += 1
		else:
			groupMaster[aVal][bVal] = 1
			
	printSeparator("Grouping by: " + groupA + " and " + groupB)
	printJSONPretty(groupMaster)
	return groupMaster

# Group by the column specified in groupA 
# Will print and return the counts of the element
def printCount(connections, groupA):
	groupMaster = {}
	timeFormatShort = "%Y/%m/%d"
	for c in connections:
		#Sanitize username, idle_since, name
		sanitizedOutput = {}
		sanitizedOutput["username"] = getUsername(c)
		sanitizedOutput["idle_since"] = getLocalTime(c)
		sanitizedOutput["name"] = getName(c)
		
		aVal = ""
		
		if groupA in c:
			aVal = c[groupA]
			
		if groupA in sanitizedOutput:
			aVal = sanitizedOutput[groupA]
		
		# Add to group count			
		if aVal in groupMaster:
			groupMaster[aVal] += 1
		else:
			groupMaster[aVal] = 1
			
	printSeparator("Grouping by: " + groupA)
	printJSONPretty(groupMaster)
	return groupMaster

#remove the {host}_all_queues_output.json
print "done"
#os.remove(host+"_all_queues_output.json")

# Output the actual groupings that you want to see
#printJSONPretty(allConnections)
#printGroupBy(allConnections, "connected_at", "node")
#printGroupBy(allConnections, "connected_at", "clean_name")
#printGroupBy(allConnections, "connected_at", "username")
#printGroupBy(allConnections, "vhost", "user")
#printGroupBy(allConnections, "connected_at", "channels")
#printCount(allConnections, "connected_at")
#printGroupBy(allConnections, "username", "idle_since")
#printCount(allConnections, "username")
#printCount(allConnections, "username")
#printCount(allConnections, "channels")
