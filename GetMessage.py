#!/usr/bin/env python
# GetMessage.py
import requests
import time
import sys
import json
from Util import *
# defining the api-endpoint 
#ENDPOINT="localhost:8080"

if len(sys.argv) == 2 and sys.argv[1] == '-h':
    showUsage("Returns the speed of Getting messages from a queue",sys.argv[0], "<host> [<vHost/queue> | <queue>]")
    print "---example: python "+sys.argv[0]+" us-west-2-dev dev/psp.out.replicator.javiertest"
    exit()

ExpectedArguments( len(sys.argv), sys.argv[0], 3)

if len(sys.argv) >= 2:
    cluster = sys.argv[1]
    host = cluster + ".perspectium.net"

queue, vHost = getQueueVirtualHost(sys.argv[2])
authenticateRabbitCredentials(cluster, vHost)
print "Host:        " + host
print "vHost:       " + vHost
print "queue:       " + queue
print "username:    " + getRabbitUser()
print "password:    " + getRabbitPassword()

Proceed()
print "loading..."

API_ENDPOINT = "https://" + host + "/output/" + queue
HEADERS = {"Content-Type": "text/json", "psp_input_queue": queue, "psp_quser" : getRabbitUser(), "psp_qpassword": getRabbitPassword()}
#HEADERS = {"Content-Type": "text/json", "psp_input_queue": queue, "psp_quser" : getRabbitUser(), "psp_qpassword": getRabbitPassword(), "use_basic_consume": "true", "max_row":"3000"}

print("URL:\n" + API_ENDPOINT)
print("Headers:\n" + str(HEADERS))

# sending post request and saving response as response object
startTime = time.time()
r = requests.get(url = API_ENDPOINT, headers=HEADERS)
endTime = time.time()

# extracting response text 
responseText = r.content

# Log Output
print("Response of Post: " + str(r))
print("Response Content:")

dataPrintLength = 1000
if len(responseText) > dataPrintLength:
	print("Response Excerpt:")
	print(responseText[0:dataPrintLength/2] + "\n.\n.\n.\n" + responseText[-dataPrintLength/2:])
else:
	print(responseText)
print("Duration of Get: " + str(endTime - startTime) + " seconds")
print("Length of Body: " + str(len(responseText) / 1000) + " KB")

dataObj = json.loads(responseText)
messagePerSecond = len(dataObj) / (endTime - startTime)

print("Number of Messages: " + str(len(dataObj)))
print("Messages per Second: " + str(messagePerSecond))
