#!/usr/bin/env python
# ParseOutQueue.py
#   Go to the queue specified in the parameters and consume it all down to a file.
#   It will be separated out into two files based on messages matching the keyword
#   specified. It will then write that data back into the two queues specified.
#
#   Messages with matching keyword go into the aux queue / file, parsing them out
#
#   All the data will be stored in files as backups. You can read/write from the
#   same queue.
import time
import sys
import pika
sys.path.append("/Library/Frameworks/Python.framework/Versions/3.5/lib/python3.5/site-packages")

# Parameters
queue ="psp.out.servicenow.example"
mainQueue = "psp.out.servicenow.example"
auxQueue  = "psp.out.servicenow.example.bulk"

mainWrite = True
auxWrite  = True

keyword = "badkey"

host     = "us-west-2-dev.perspectium.net"
vHost    = "/"
username = "support"
password = "psp123"
port     = 5672
myExchange = ""
credentials = pika.PlainCredentials(username, password)

# Files
originalFileName = queue + "_original_file.txt"
mainFileName = queue + "_main_file.txt"
auxFileName  = queue + "_aux_file.txt"

originalFile = open(originalFileName, "w")
mainFile = open(mainFileName, "w")
auxFile  = open(auxFileName, "w")

# Print diagnostic info
print "Parsing data from: " + vHost + " " + queue + " surrounding the keyword: " + keyword
print "Data will be written to files: " + originalFileName + ", " + mainFileName + ", " + auxFileName
if mainWrite:
    print "Data not matching the keyword will be written back to: " + mainQueue
if auxWrite:
    print "Data matching the keyword will be written back to: " + auxQueue

raw_input('Press enter to continue')

# callback for basic consume
def callback(ch, method, properties, body):
    #print body
    originalFile.write(body)
    originalFile.write("\n")
    if keyword in body:
        auxFile.write(body)
        auxFile.write("\n")
    else:
        mainFile.write(body)
        mainFile.write("\n")

# Read from queue
connection = pika.BlockingConnection(pika.ConnectionParameters(host, port, vHost, credentials))
channel = connection.channel()
channel.basic_consume(callback,queue, "True")

print(' [*] Waiting for messages. To exit press CTRL+C')
try:
    channel.start_consuming()
except KeyboardInterrupt:
    channel.stop_consuming()
    sys.stdout.flush()
 
try:
    connection.close()
except Exception as exception:
    pass

originalFile.close()
mainFile.close()
auxFile.close()

print "Done Consuming"

# Write back to queues
connectionMain = pika.BlockingConnection(pika.ConnectionParameters(host, port, vHost, credentials))
connectionAux  = pika.BlockingConnection(pika.ConnectionParameters(host, port, vHost, credentials))
channelMain = connectionMain.channel()
channelAux  = connectionAux.channel()

index = 0
cap = 1000000

if mainWrite :
    print "Writing back to: " + mainQueue
    mainFile = open(mainFileName, 'r')
    with mainFile as fM:
        for line in fM:
            if index >= cap:
                break
            elif line not in ['\n', '\r\n']:
                channelMain.basic_publish(exchange=myExchange,routing_key=mainQueue,body=line)
                index = index + 1
                if index % 1000 == 0:
                    print index
                elif index % 200 == 0:
                    print ".",
                    sys.stdout.flush()
sys.stdout.flush()
 
index = 0
cap = 1000000

if auxWrite :
    print "Writing back to: " + auxQueue
    auxFile = open(auxFileName, 'r')
    with auxFile as fA:
        for line in fA:
            if index >= cap:
                break
            elif line not in ['\n', '\r\n']:
                channelMain.basic_publish(exchange=myExchange,routing_key=auxQueue,body=line)
                index = index + 1
                if index % 1000 == 0:
                    print index
                elif index % 200 == 0:
                    print ".",
                    sys.stdout.flush()

connectionMain.close()
connectionAux.close()

print "Finished"