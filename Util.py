#!/usr/bin/env python

#  Util.py
#  Contains shared functions called from different scripts

import sys
from sys      import argv
import os
sys.path.append("/Library/Frameworks/Python.framework/Versions/3.5/lib/python3.5/site-packages")
import pika
import subprocess

# Global Variables
PORT=5672
CLUSTER=""
DEFAULT_RABBIT_USER="admin"
DEFAULT_RABBIT_PASSWORD="adminadmin"
RABBIT_USER=""
RABBIT_PASSWORD=""
RABBIT_VHOST="/"
RABBIT_SERVLET=":15672/api/"
MBS_MULTIINPUT="/multiinput"
HTTPS="https://"

def authenticateRabbitCredentials(fCluster, fVirtualHost):
	global RABBIT_USER, RABBIT_PASSWORD, RABBIT_CHANNEL, RABBIT_CONNECTION, RABBIT_VHOST, CLUSTER

	CLUSTER=fCluster
	RABBIT_VHOST=fVirtualHost

	p = subprocess.Popen(["./getRabbitCredentials.sh", CLUSTER], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	output = p.communicate()[0]

	RABBIT_USER = output.split("\n")[0]

	if "/" in RABBIT_USER:
		RABBIT_USER = RABBIT_USER.split("/")[1]

	# Check
	if len(output) == 0:
		print "Unable to log in. Please check your vpn or username\n"
		exit()
	if len(output.split("\n")) < 2: #should be returning 3
		print "Please check for the file /etc/ansible/host on the cluster-monitor\n"
		exit()
	else:
		RABBIT_PASSWORD = output.split("\n")[1]

	try:
		print CLUSTER
		credentials = pika.PlainCredentials(RABBIT_USER, RABBIT_PASSWORD)
		RABBIT_CONNECTION = pika.BlockingConnection(pika.ConnectionParameters(getHostFullURL(CLUSTER), PORT, RABBIT_VHOST, credentials))
		RABBIT_CHANNEL = RABBIT_CONNECTION.channel()

		print RABBIT_USER
		print RABBIT_PASSWORD

		if RABBIT_CHANNEL.is_open == True:
			print "Success: RabbitMQ Authentication"
			return

	except Exception as exception:
		print "Failure: RabbitMQ Authentication. Check your credentials"
		pass

def connectionTimeoutCallback():
	print("connecionClosedCallback")
	exit()

def getRabbitUser():
	return RABBIT_USER

def getRabbitPassword():
	return RABBIT_PASSWORD

def getRabbitChannel():
	return RABBIT_CHANNEL

def getRabbitConnection():
	return RABBIT_CONNECTION

def getRabbitServlet():
    return RABBIT_SERVLET

def getRabbitDefinitionServlet():
    return RABBIT_SERVLET + "definitions"

def getRabbitQueuesServlet():
	return RABBIT_SERVLET + "queues"

def getRabbitConnectionsServlet():
    return RABBIT_SERVLET + "connections"

def getMBSMultiInputAPI():
	return HTTPS + getHostFullURL(CLUSTER) + MBS_MULTIINPUT

def getQueueVirtualHost(queue):
	vHost = "/"
	if "/" in queue:
	    index = queue.find("/")
	    vHost = queue[:index]
	    queue = queue[index+1:]
	return queue, vHost

def getHostFullURL(host):
	return host + ".perspectium.net"

def showUsage(header, programName, parameters):
	print "******************************************************************"
	print header
	print "******************************************************************"
	print "--- usage: Python " + programName + " " + parameters + "\n"

def printBreakdown(theValues, keyType):
	print "\n> Breakdown by " + keyType
	keys = theValues.viewitems()
	for key in sorted(keys):
		print key[0] + " = " + str(key[1])

def countMessage(keyValue, TheList):
	if keyValue in TheList:
		TheList[keyValue] += 1
	else:
		TheList[keyValue] = 1

def ExpectedArguments(argv, programName, number):
	if argv < number:
		print "Expected " + str(number) + " arguments for " + programName
		exit()

def Proceed():
	answer = raw_input("\nProceed? (Y/N)	")
	if (answer.lower() != 'yes' and answer.lower() != 'y'):
		exit()
"""
def auth_rabbitmq() {
	credentials = pika.PlainCredentials(getRabbitUser(), getRabbitPassword())
	connection = pika.BlockingConnection(pika.ConnectionParameters(host, port, vHost, credentials))
	channel = connection.channel()

	returns true | false


}


def getRabbitCredentials():
	username = "username"
	password = "password"

	print username, password


if __name__ == "__main__":
	main()
"""
