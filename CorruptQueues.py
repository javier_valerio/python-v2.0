#!/usr/bin/python
# Search through RabbitMQ Queues to find "corrupted queues"
#   This is done by looking for queues with 
#		- no meta data 
#         OR
#		- an ha policy set
#		- no slave nodes
#   This will generally fail on single rabbit node clusters since there won't be any slave
#   nodes for a queue to have.
#
#   Call by passing in the cluster and -v for verbose info
#     ex: ./CorruptQueues.py us-east-1-c2 -v
import urllib, json
import sys

def queueNoMetadata(x):
	return "policy" not in x and "memory" not in x

def noSlavesOnHAQueue(x):
	return x["policy"] is not None and x["slave_nodes"] == []

def printJSONPretty(jsonObj):
	print json.dumps(jsonObj, indent=4, sort_keys=True)

# Check to see if a cluster is specified
if len(sys.argv) < 2:
	print "Specify a cluster and optional flag -v for verbose information"
	print "Example: Python CorruptQueues.py us-west-2-c2 -v"
	sys.exit(0)

# Verbose messages
verbose = False
if(len(sys.argv) >= 3 and  sys.argv[2] == "-v"):
	verbose = True
 
# Parameters for data request  
username='admin'
password='adminadmin'
#username='mbs'
#password='I#E0Hl4m7NW85yk'
cluster = sys.argv[1]
path=".perspectium.net:15672/api/queues"

# Hit URL for JSON queues 
try:
	url = "http://" + username + ":" + password + "@" + cluster + path
	if verbose:
		print "Grabbing corrupted queues for: " + cluster
	response = urllib.urlopen(url)
	data = json.loads(response.read())
except Exception, e:
	print "EXITING ERROR DURING DATA REQUEST for: ", url, "\n", e
	sys.exit(0)

# Run through the data set and perform the checks
queuesLength = len(data)
index = 0
corruptCount = 0
for x in data:
	#print index, x
	if queueNoMetadata(x):
		if(verbose):
			queueIndexStr = "#" + str(corruptCount) + " (" + str(index) + "/" + str(queuesLength) + ") : "
			print queueIndexStr, x["vhost"], "/", x["name"], x["node"], "has no metadata"
			printJSONPretty(x)
		else:
			print x["vhost"], x["name"]
		corruptCount = corruptCount + 1;
	elif noSlavesOnHAQueue(x):
		if(verbose):
			queueIndexStr = "#" + str(corruptCount) + " (" + str(index) + "/" + str(queuesLength) + ") : "
			print queueIndexStr, x["vhost"], "/", x["name"], x["node"], "has no slaves on HA queue"
			printJSONPretty(x)
		else:
			print x["vhost"], x["name"]
		corruptCount = corruptCount + 1;
	elif x["name"] == "psp.in.replicator" and x["vhost"] == "westmonroepartners":
		printJSONPretty(x)
	index = index + 1
	
print corruptCount, "Corrupted Queues on the cluster: ", cluster
